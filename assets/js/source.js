
MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  // Select the node that will be observed for mutations
const targetNode = document.getElementsByTagName('body');

// Options for the observer (which mutations to observe)
const config = {
  subtree: true,
  childList: true,
};

// Callback function to execute when mutations are observed
const callback = function(mutationsList, observer) {
  for(let mutation of mutationsList) {
    if (mutation.type == "childList") {
      let elms = $('.userContentWrapper:not([phone-ext=true])');
      let lastCounter = elms.length;
      for (var i = 0; i < lastCounter; i++) {
        if(!$(elms[i]).attr('phone-ext')) {
          $(elms[i]).attr("phone-ext", true);
          $(elms[i]).append('<button class="get_mail">GET MAIL</button>');
        }
      }
    }
  }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);

observer.observe(document, config);

$(document).ready(function () {
  try {

    setInterval(function () {

      $('div[data-testid^="UFI2Comment/root"] ._42ef').each(function (t, n) {
        //console.log($(n));
        if (!$(n).hasClass("nhd_added")) {
          var a = $(n).find('ul[data-testid^="UFI2CommentActionLinks/root"]');
          if (null != a) {
            var e = $(n).find('div[data-testid^="UFI2Comment/body"]').find('a[data-hovercard^="/ajax/hovercard"]'),
              o = $(e).data("hovercard").split("?id=")[1].split("&extragetparams")[0],
              d = $(e).text(),
              s = btoa($(n).find(".timestampContent").parent().parent().attr("href"));
            $(a).append('<li class="_6coj"><span aria-hidden="true" class="_6cok">&nbsp;·&nbsp;</span> <a data-fbid="' + o + '" data-fbname="' + d + '" data-fbsource="comment" data-fblink="' + s + '" class="nhd_btn_get_phone nhd_' + o + '"  href="#"><img  style="width: 30px;position: absolute;top: 0;right: 10px;" src="https://lh3.googleusercontent.com/hJE5dNVr-0cLzAmrLnrUIpIoVwfhDeIqIDer0FTJdvve5CxF6yuW-gjJpVbh8p0cEXxpcuE0RExdxAVrww=s328-no"/></a></li>'), $(n).addClass("nhd_added")
          }
        }
      })
    }, 500)
  } catch (t) {}
 

}), $(document).on("click", ".nhd_btn_get_phone", function (t) {
  function formatUsPhone(text) {
    return text.replace(/(\d{3})(\d{3})(\d{4})/, "$1.$2.$3");
  }
  let seft = $(this);
  $(this).addClass('show-sdt');

  let fbId = $(this).attr('data-fbid');
  seft.text('Đang tìm kiếm...');

  chrome.extension.sendRequest({
    check: 'getphone',
    id: fbId
  }, function (response) {
    console.log(response)
    if (response.status == 2) {
      seft.text('Vui lòng đăng nhập để sử dụng dịch vụ !');
      seft.css('color', '#dc3545');
    } else if (response.status == 1) {
      seft.text(formatUsPhone(response.phone));
    } else if (response.message == 'Licenses Hết Hạn!'){
      seft.html('Tài khoản đã hết hạn vui lòng đăng ký <br> để sử dụng dịch vụ !');
      seft.css('color', '#dc3545');
    } else if (response.message == 'Licenses bị khóa'){
      seft.html('Tài khoản findCall của bạn đã bị khóa, <br>hãy liên hệ với admin để mở khóa và sử dụng dịch vụ nhé !');
      seft.css('color', '#dc3545');

    }
     else {
      seft.text('Người dùng này không có số điện thoại !');
      seft.css('color', '#dc3545');
    }

  });

}),$(document).on("click",".get_mail",function(t,v){
  var list_phone = [];
  var list_name = [];
  //$(this).find('._6qw4').text()
  //$(this).find('._3l3x').text()
  var regex_email = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/gmi;


});


